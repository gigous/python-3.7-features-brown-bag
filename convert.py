import subprocess
from time import sleep
from datetime import datetime as dt

while True:
    subprocess.run(["jupyter", "nbconvert", "Python3.7_plus_features.ipynb", "--to", "slides"])
    # subprocess.run(["jupyter"])
    print(f"{dt.now()} converted to slides")
    sleep(10)
